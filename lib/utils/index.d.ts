import { ChromePageOptions, ChromeNetworkOptions, IChromeOptionSetter } from '../types';
export declare function firstNonNegative(values: any): any;
export declare function toMilliseconds(time: any): number;
export declare function convertQueryString(fullUrl: any): any[];
export declare function convertHeaders(headers: any): {
    'pairs': any[];
    'size': any;
};
export declare function configurePage(pageOptions?: ChromePageOptions): IChromeOptionSetter;
export declare function configurNetwork(networkOptions?: ChromeNetworkOptions): IChromeOptionSetter;
export declare function configureChrome(chrome: any, functions?: Array<IChromeOptionSetter>): Promise<{}>;
