"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var url = require('url');
function firstNonNegative(values) {
    for (var i = 0; i < values.length; i++) {
        if (values[i] >= 0) {
            return values[i];
        }
    }
    return -1;
}
exports.firstNonNegative = firstNonNegative;
function toMilliseconds(time) {
    return time === -1 ? -1 : time * 1000;
}
exports.toMilliseconds = toMilliseconds;
function convertQueryString(fullUrl) {
    var query = url.parse(fullUrl, true).query;
    var pairs = [];
    for (var name in query) {
        var value = query[name];
        pairs.push({ 'name': name, 'value': value.toString() });
    }
    return pairs;
}
exports.convertQueryString = convertQueryString;
//content loaded 481
function convertHeaders(headers) {
    let headersObject = { 'pairs': [], 'size': undefined };
    if (Object.keys(headers).length) {
        headersObject.size = 2; // trailing "\r\n"
        for (var name in headers) {
            var value = headers[name];
            headersObject.pairs.push({ 'name': name, 'value': value });
            headersObject.size += name.length + value.length + 4; // ": " + "\r\n"
        }
    }
    return headersObject;
}
exports.convertHeaders = convertHeaders;
function configurePage(pageOptions = {}) {
    return function setPage(chrome) {
        return new Promise((resolve, reject) => {
            chrome.Page.enable();
            if (!pageOptions.ChromePageDeviceMetricsOverride) {
                return resolve();
            }
            chrome.Page.setDeviceMetricsOverride(pageOptions.ChromePageDeviceMetricsOverride, (e, r) => {
                if (e) {
                    return reject(e);
                }
                return resolve(r);
            });
        });
    };
}
exports.configurePage = configurePage;
function configurNetwork(networkOptions = {}) {
    return function setNetwork(chrome) {
        return new Promise((resolve, reject) => {
            chrome.Network.enable();
            chrome.Network.setCacheDisabled({ 'cacheDisabled': true });
            if (networkOptions.userAgent) {
                chrome.Network.setUserAgentOverride({ 'userAgent': networkOptions.userAgent });
            }
        });
    };
}
exports.configurNetwork = configurNetwork;
function configureChrome(chrome, functions = []) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Promise.all(functions.map(m => {
                m(chrome);
            })).then(() => {
                chrome.once('ready', function () {
                    resolve(chrome);
                });
            }).catch(reject);
        });
    });
}
exports.configureChrome = configureChrome;
