"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const screenshot_1 = require("../screenshot");
const har_1 = require("../har");
const screencast_1 = require("../screencast");
function init() {
    function foo(req, res, next) {
        return res.send('foo2');
    }
    function bar(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield screenshot_1.captureScreenshot('http://www.bbc.co.uk/');
            return res.send(result);
        });
    }
    function har(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            // let url = req.query.url || '';
            // if (url){
            //   res.send('url is required').status(500);
            // }
            let result = yield har_1.getHar('http://www.bbc.co.uk/');
            return res.send(result);
        });
    }
    function captureScreencast(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            // let url = req.query.url || '';
            // if (url){
            //   res.send('url is required').status(500);
            // }
            let result = yield screencast_1.captureScreencastAsync('http://www.bbc.co.uk/');
            return res.send(result);
        });
    }
    return {
        routes: (router) => {
            router.get('/foo', foo);
            router.get('/bar', bar);
            router.get('/har', har);
            router.get('/screencast', captureScreencast);
        }
    };
}
exports.init = init;
;
