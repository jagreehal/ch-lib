/// <reference types="express" />
import * as express from "express";
export declare function init(): {
    routes: (router: express.Router) => void;
};
