export interface Chrome {
    Page: any;
    Network: any;
    Emulation: any;
}
export interface IChromeTask<T> {
    (chrome: Chrome, url: string, options: Array<IChromeOptionSetter>): Promise<T>;
}
export interface IChromeOptionSetter {
    (chrome: Chrome): Promise<any>;
}
export interface PageOptions {
    fetchContent: boolean;
    onLoadDelay: number;
    force: boolean;
}
export interface ChromePageDeviceMetricsOverride {
    width: number;
    height: number;
    deviceScaleFactor: number;
    mobile: boolean;
    fitWindow: boolean;
}
export interface ChromePageOptions {
    ChromePageDeviceMetricsOverride?: ChromePageDeviceMetricsOverride;
}
export interface ChromeNetworkOptions {
    userAgent?: string;
}
export interface Success<T> {
    success: true;
    value: T;
}
export interface Failure {
    success: false;
    reason: string;
}
export declare type Result<T> = Success<T> | Failure;
