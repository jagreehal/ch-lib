"use strict";
var screenshot_1 = require("./screenshot");
exports.captureScreenshot = screenshot_1.captureScreenshot;
var routes_1 = require("./routes");
exports.init = routes_1.init;
