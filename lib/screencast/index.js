"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const utils_1 = require("../utils");
const Chrome = require('chrome-remote-interface');
function captureScreencastAsync(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            var rawEvents = [];
            let chromeNetworkOptions = utils_1.configurNetwork();
            var deviceMetrics = {
                width: 1000,
                height: 1000,
                deviceScaleFactor: 1,
                scale: 1,
                mobile: true,
                fitWindow: true,
                screenWidth: 1000,
                screenHeight: 1000
            };
            let chromePageOptions = utils_1.configurePage({ ChromePageDeviceMetricsOverride: deviceMetrics });
            Chrome({}, function (chrome) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield utils_1.configureChrome(chrome, [chromePageOptions, chromeNetworkOptions]);
                    chrome.Page.screencastFrame(data => {
                        rawEvents.push(data);
                    });
                    let x = {
                        quality: 100,
                        maxWidth: deviceMetrics.width,
                        maxHeight: deviceMetrics.height,
                        everyNthFrame: 1
                    };
                    chrome.Page.startScreencast(x);
                    chrome.Page.navigate({ 'url': url });
                    chrome.Page.loadEventFired(() => {
                        setTimeout(() => {
                            // rawEvents.push(chrome.Page.captureScreenshot())
                            chrome.Page.stopScreencast();
                            chrome.close();
                            return resolve(rawEvents);
                        }, 1000);
                    });
                });
            }).on('error', reject);
        });
    });
}
exports.captureScreencastAsync = captureScreencastAsync;
