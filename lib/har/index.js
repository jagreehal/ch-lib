"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var Chrome = require('chrome-remote-interface');
const page_1 = require("../page");
const utils_1 = require("../utils");
function getHar(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let chromePageOptions = utils_1.configurePage({});
            let chromeNetworkOptions = utils_1.configurNetwork({});
            let pageOptions = {
                fetchContent: true,
                onLoadDelay: 0,
                force: false
            };
            Chrome({}, function (chrome) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        yield utils_1.configureChrome(chrome, [chromePageOptions, chromeNetworkOptions]);
                        var pageData = yield getPageData(chrome, url, pageOptions);
                        resolve(pageData);
                    }
                    catch (e) {
                        reject(e);
                    }
                    chrome.close();
                });
            });
        });
    });
}
exports.getHar = getHar;
function getPageData(chrome, url, options) {
    return __awaiter(this, void 0, void 0, function* () {
        const NEUTRAL_URL = 'about:blank';
        var PAGE_DELAY = 1000;
        return new Promise((resolve, reject) => {
            var page = new page_1.Page(url, chrome, options.fetchContent);
            var loadEventTimeout;
            // load a neutral page before the user provided URL since
            // there's no way to stop pending loadings using the protocol
            chrome.Page.navigate({ 'url': NEUTRAL_URL }, function (error, response) {
                if (error) {
                    reject(new Error('Cannot load URL'));
                }
            });
            // wait its completion before starting with the next user-defined URL
            var neutralFrameid;
            chrome.on('event', function (message) {
                switch (message.method) {
                    case 'Page.frameNavigated':
                        // save the frame id of the neutral URL
                        var frame = message.params.frame;
                        if (frame.url === NEUTRAL_URL) {
                            neutralFrameid = frame.id;
                        }
                        break;
                    case 'Page.frameStoppedLoading':
                        // load the next URL when done
                        if (message.params.frameId === neutralFrameid) {
                            chrome.removeAllListeners('event');
                            // inject the JavaScript code and load this URL
                            //                            self.emit('pageStart', url); log here!
                            chrome.Network.clearBrowserCache();
                            chrome.Page.navigate({ 'url': url }, function (error, response) {
                                if (error) {
                                    return reject(new Error('Cannot load URL'));
                                }
                            });
                            // then process events
                            chrome.on('event', function (message) {
                                page.processMessage(message);
                                // check if done with the current URL
                                if (page.isFinished() && typeof loadEventTimeout === 'undefined') {
                                    // keep listening for events for a certain amount of time
                                    // after the load event is triggered
                                    loadEventTimeout = setTimeout(function () {
                                        // self.emit(page.isFailed() ? 'pageError' : 'pageEnd', url);
                                        chrome.removeAllListeners('event');
                                        // start the next URL after a certain delay
                                        // so to "purge" any spurious requests
                                        setTimeout(function () {
                                            return resolve(page.getData());
                                        }, PAGE_DELAY);
                                    }, options.onLoadDelay);
                                }
                            });
                        }
                        break;
                }
            });
        });
    });
}
