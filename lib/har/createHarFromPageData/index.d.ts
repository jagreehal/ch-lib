export default function createHarFromPagepageData(pageData: any): {
    'info': {
        'startedDateTime': string;
        'title': any;
        'pageTimings': {
            'onContentLoad': number;
            'onLoad': number;
        };
    };
    'entries': any[];
};
