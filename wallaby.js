module.exports = function (wallaby) {
  return {
    files: [
      { pattern: 'node_modules/**', 'ignore': true },
      { pattern: 'lib/**/*.json', load: false },
      '!src/**/*.spec.ts',
      'src/**/*.ts'
    ],
    tests: [
      'src/**/*.spec.js',
      'src/**/*.spec.ts'
    ],
    env: {
      type: 'node'
    },
    compilers: {
      '**/*.ts': wallaby.compilers.typeScript({ typescript: require('typescript') })
    },
    preprocessors: {
      '**/*.spec.js': file => {
        var getSourceMapFromDataUrl = function (code) {
          const sourceMapCommentRegEx = /\/\/[@#] sourceMappingURL=data:application\/json(?:;charset[:=][^;]+)?;base64,(.*)\n/;
          const match = code.match(sourceMapCommentRegEx);
          const sourceMapBase64 = match[1];
          return JSON.parse(new Buffer(sourceMapBase64, 'base64').toString());
        };
        var transformedCode = require('espower-source')(
          file.content.replace('(\'assert\')', '(\'power-assert\')'),
          file.path);

        return { code: transformedCode, sourceMap: getSourceMapFromDataUrl(transformedCode) };
      }
    },
    setup: function () {
      var Module = require('module').Module;
      if (!Module._originalRequire) {
        const modulePrototype = Module.prototype;
        Module._originalRequire = modulePrototype.require;
        modulePrototype.require = function (filePath) {
          if (filePath === 'empower-core') {
            var originalEmpowerCore = Module._originalRequire.call(this, filePath);
            var newEmpowerCore = function () {
              var originalOnError = arguments[1].onError;
              arguments[1].onError = function (errorEvent) {
                errorEvent.originalMessage = errorEvent.error.message + '\n';
                return originalOnError.apply(this, arguments);
              };
              return originalEmpowerCore.apply(this, arguments);
            };
            newEmpowerCore.defaultOptions = originalEmpowerCore.defaultOptions;
            return newEmpowerCore;
          }
          return Module._originalRequire.call(this, filePath);
        };
      }
    }
  };
};
