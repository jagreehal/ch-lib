var Chrome = require('chrome-remote-interface');

import * as types from '../types';

import { configurePage, configurNetwork, configureChrome, convertHeaders, convertQueryString, firstNonNegative, toMilliseconds } from '../utils';

export async function captureScreenshot(url)  {
 return new Promise((resolve, reject) => {
    var rawEvents = [];
    let chromeNetworkOptions = configurNetwork();

    var deviceMetrics = {
      width: 320,
      height: 600,
      deviceScaleFactor: 1,
      scale: 1,
      mobile: true,
      fitWindow: false
    };

    let chromePageOptions = configurePage({ ChromePageDeviceMetricsOverride: deviceMetrics });

    Chrome({}, async function (chrome) {
      await configureChrome(chrome, [chromePageOptions, chromeNetworkOptions]);

      let x = {
        quality: 100,
        maxWidth: deviceMetrics.width,
        maxHeight: deviceMetrics.height,
        everyNthFrame: 1
      }

      chrome.Page.startScreencast(x);
      chrome.Page.navigate({ 'url': url })
      chrome.Page.loadEventFired(() => {
        setTimeout(() => {
          chrome.Page.captureScreenshot().then(s => {
            var data = s.data.replace(/^data:image\/\w+;base64,/, '');
            rawEvents.push(data);
            chrome.close();
            return resolve(rawEvents);
          });
        }, 1000);
      });
    }).on('error', e => {
      reject(e);
    });
  })
}
