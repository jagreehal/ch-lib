import * as types from '../../types';

import {configurePage, configurNetwork, configureChrome, convertHeaders, convertQueryString, firstNonNegative, toMilliseconds } from '../../utils';

export default function createHarFromPagepageData(pageData) {
  // page timings
  var wallTime = pageData.objects[pageData.originalRequestId].requestMessage.wallTime;
  var startedDateTime = new Date(wallTime * 1000).toISOString();
  var onContentLoad = pageData.domContentEventFiredMs - pageData.originalRequestMs;
  var onLoad = pageData.loadEventFiredMs - pageData.originalRequestMs;
  // entries
  var entries = [];
  for (var requestId in pageData.objects) {
    var object = pageData.objects[requestId];
    // skip incomplete entries, those that have no timing information (since
    // it's optional) or data URI requests
    if (!object.responseMessage || !object.responseFinished ||
      !object.responseMessage.response.timing ||
      object.requestMessage.request.url.match('^data:')) {
      continue;
    }
    // check for redirections
    var redirectUrl = '';
    if (object.requestMessage.redirectResponse) {
      redirectUrl = object.requestMessage.redirectResponse.url;
    }
    // process headers
    var requestHeaders = convertHeaders(
      object.responseMessage.response.requestHeaders ||
      object.requestMessage.request.headers);
    var responseHeaders = convertHeaders(object.responseMessage.response.headers);
    // add status line length (12 = "HTTP/1.x" + "  " + "\r\n")
    requestHeaders.size += (object.requestMessage.request.method.length +
      object.requestMessage.request.url.length + 12);
    responseHeaders.size += (object.responseMessage.response.status.toString().length +
      object.responseMessage.response.statusText.length + 12);
    // query string
    var queryString = convertQueryString(object.requestMessage.request.url);
    // object timings
    // https://chromium.googlesource.com/chromium/blink.git/+/master/Source/devtools/front_end/sdk/HAREntry.js
    var timing = object.responseMessage.response.timing;
    var duration = object.responseFinished - timing.requestTime;
    var blockedTime = firstNonNegative([timing.dnsStart, timing.connectStart, timing.sendStart]);
    var dnsTime = -1;
    if (timing.dnsStart >= 0) {
      dnsTime = firstNonNegative([timing.connectStart, timing.sendStart]) - timing.dnsStart;
    }
    var connectTime = -1;
    if (timing.connectStart >= 0) {
      connectTime = timing.sendStart - timing.connectStart;
    }
    var sendTime = timing.sendEnd - timing.sendStart;
    var waitTime = timing.receiveHeadersEnd - timing.sendEnd;
    var receiveTime = toMilliseconds(duration) - timing.receiveHeadersEnd;
    var sslTime = -1;
    if (timing.sslStart >= 0 && timing.sslEnd >= 0) {
      sslTime = timing.sslEnd - timing.sslStart;
    }
    // connection information
    var serverIPAddress = object.responseMessage.response.remoteIPAddress;
    var connection = object.responseMessage.response.connectionId;
    // sizes
    var bodySize = object.encodedResponseLength - responseHeaders.size;
    var compression = object.responseLength - bodySize;
    // HTTP version or protocol name (e.g., quic)
    var protocol = object.responseMessage.response.protocol || 'unknown';
    // fill entry
    entries.push({
      'startedDateTime': new Date(object.requestMessage.wallTime * 1000).toISOString(),
      'time': toMilliseconds(duration),
      'request': {
        'method': object.requestMessage.request.method,
        'url': object.requestMessage.request.url,
        'httpVersion': protocol,
        'cookies': [], // TODO
        'headers': requestHeaders.pairs,
        'queryString': queryString,
        'headersSize': requestHeaders.size,
        'bodySize': object.requestMessage.request.headers['Content-Length'] || -1,
      },
      'response': {
        'status': object.responseMessage.response.status,
        'statusText': object.responseMessage.response.statusText,
        'httpVersion': protocol,
        'cookies': [], // TODO
        'headers': responseHeaders.pairs,
        'redirectURL': redirectUrl,
        'headersSize': responseHeaders.size,
        'bodySize': bodySize,
        '_transferSize': object.encodedResponseLength,
        'content': {
          'size': object.responseLength,
          'mimeType': object.responseMessage.response.mimeType,
          'compression': compression,
          'text': object.responseBody,
          'encoding': object.responseBodyIsBase64 ? 'base64' : undefined,
        }
      },
      'cache': {},
      'timings': {
        'blocked': blockedTime,
        'dns': dnsTime,
        'connect': connectTime,
        'send': sendTime,
        'wait': waitTime,
        'receive': receiveTime,
        'ssl': sslTime
      },
      'serverIPAddress': serverIPAddress,
      'connection': connection.toString(),
      '_initiator': object.requestMessage.initiator
    });
  }
  // outcome
  return {
    'info': {
      'startedDateTime': startedDateTime,
      'title': pageData.url,
      'pageTimings': {
        'onContentLoad': onContentLoad,
        'onLoad': onLoad
      }
    },
    'entries': entries
  };
}
