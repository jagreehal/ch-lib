import * as fs from 'fs';
import * as types from '../types';

var Chrome = require('chrome-remote-interface');

import { Page } from '../page';
import { configurePage, configurNetwork, configureChrome, convertHeaders, convertQueryString, firstNonNegative, toMilliseconds }  from '../utils';

export async function getHar(url: string) {
  return new Promise((resolve, reject) => {
    let chromePageOptions = configurePage({});
    let chromeNetworkOptions = configurNetwork({});
    let pageOptions: types.PageOptions = {
      fetchContent: true,
      onLoadDelay: 0,
      force: false
    }

    Chrome({}, async function (chrome) {
      try {
        await configureChrome(chrome, [chromePageOptions, chromeNetworkOptions]);
        var pageData = await getPageData(chrome, url, pageOptions);
        resolve(pageData);
      }
      catch (e) {
        reject(e);
      }
      chrome.close();
    });
  })
}

async function getPageData(chrome, url: string, options: types.PageOptions) {
  const NEUTRAL_URL = 'about:blank';
  var PAGE_DELAY = 1000;

  return new Promise((resolve, reject) => {

    var page = new Page(url, chrome, options.fetchContent);
    var loadEventTimeout;
    // load a neutral page before the user provided URL since
    // there's no way to stop pending loadings using the protocol
    chrome.Page.navigate({ 'url': NEUTRAL_URL }, function (error, response) {
      if (error) {
        reject(new Error('Cannot load URL'));
      }
    });
    // wait its completion before starting with the next user-defined URL
    var neutralFrameid;
    chrome.on('event', function (message) {
      switch (message.method) {
        case 'Page.frameNavigated':
          // save the frame id of the neutral URL
          var frame = message.params.frame;
          if (frame.url === NEUTRAL_URL) {
            neutralFrameid = frame.id;
          }
          break;
        case 'Page.frameStoppedLoading':
          // load the next URL when done
          if (message.params.frameId === neutralFrameid) {
            chrome.removeAllListeners('event');
            // inject the JavaScript code and load this URL
            //                            self.emit('pageStart', url); log here!
            chrome.Network.clearBrowserCache();
            chrome.Page.navigate({ 'url': url }, function (error, response) {
              if (error) {
                return reject(new Error('Cannot load URL'));
              }
            });

            // then process events
            chrome.on('event', function (message) {
              page.processMessage(message);
              // check if done with the current URL
              if (page.isFinished() && typeof loadEventTimeout === 'undefined') {
                // keep listening for events for a certain amount of time
                // after the load event is triggered
                loadEventTimeout = setTimeout(function () {
                  // self.emit(page.isFailed() ? 'pageError' : 'pageEnd', url);
                  chrome.removeAllListeners('event');
                  // start the next URL after a certain delay
                  // so to "purge" any spurious requests
                  setTimeout(function () {
                    return resolve(page.getData());
                  }, PAGE_DELAY);
                }, options.onLoadDelay);
              }
            });
          }
          break;
      }
    });
  });
}

