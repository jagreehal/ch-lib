import * as types from '../types';
import { configurePage, configurNetwork, configureChrome, convertHeaders, convertQueryString, firstNonNegative, toMilliseconds } from '../utils';

const Chrome = require('chrome-remote-interface');

export async function captureScreencastAsync(url: string) {
  return new Promise((resolve, reject) => {
    var rawEvents = [];
    let chromeNetworkOptions = configurNetwork();

    var deviceMetrics = {
      width: 1000,
      height:1000,
      deviceScaleFactor: 1,
      scale:1,
      mobile: true,
      fitWindow: true,
      screenWidth:1000,
      screenHeight:1000
    };

    let chromePageOptions = configurePage({ ChromePageDeviceMetricsOverride: deviceMetrics });

    Chrome({}, async function (chrome) {
      await configureChrome(chrome, [chromePageOptions, chromeNetworkOptions]);
      chrome.Page.screencastFrame(data => {
        rawEvents.push(data);
      });

      let x = {
        quality: 100,
        maxWidth:deviceMetrics.width,
        maxHeight:deviceMetrics.height,
        everyNthFrame: 1
      }

      chrome.Page.startScreencast(x);
      chrome.Page.navigate({ 'url': url })
      chrome.Page.loadEventFired(() => {
        setTimeout(()=>{
        // rawEvents.push(chrome.Page.captureScreenshot())
        chrome.Page.stopScreencast();
        chrome.close();
        return resolve(rawEvents);
        },1000);
      });
    }).on('error', reject);
  })
}
