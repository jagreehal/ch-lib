var url = require('url');

import { ChromePageOptions, ChromeNetworkOptions, IChromeOptionSetter } from '../types';


export function firstNonNegative(values) {
  for (var i = 0; i < values.length; i++) {
    if (values[i] >= 0) {
      return values[i];
    }
  }

  return -1;
}

export function toMilliseconds(time) {
  return time === -1 ? -1 : time * 1000;
}

export function convertQueryString(fullUrl) {
  var query = url.parse(fullUrl, true).query;
  var pairs = [];
  for (var name in query) {
    var value = query[name];
    pairs.push({ 'name': name, 'value': value.toString() });
  }
  return pairs;
}
//content loaded 481
export function convertHeaders(headers) {
  let headersObject = { 'pairs': [], 'size': undefined };
  if (Object.keys(headers).length) {
    headersObject.size = 2; // trailing "\r\n"
    for (var name in headers) {
      var value = headers[name];
      headersObject.pairs.push({ 'name': name, 'value': value });
      headersObject.size += name.length + value.length + 4; // ": " + "\r\n"
    }
  }
  return headersObject;
}

export function configurePage(pageOptions: ChromePageOptions = {}) {
  return <IChromeOptionSetter>function setPage(chrome) {
    return new Promise((resolve, reject) => {
      chrome.Page.enable();
      if (!pageOptions.ChromePageDeviceMetricsOverride) {
        return resolve();
      }
      chrome.Page.setDeviceMetricsOverride(pageOptions.ChromePageDeviceMetricsOverride, (e, r) => {
        if (e) {
          return reject(e);
        }
        return resolve(r)
      });
    });
  };
}

export function configurNetwork(networkOptions: ChromeNetworkOptions = {}) {
  return <IChromeOptionSetter>function setNetwork(chrome) {
    return new Promise((resolve, reject) => {
      chrome.Network.enable();
      chrome.Network.setCacheDisabled({ 'cacheDisabled': true });
      if (networkOptions.userAgent) {
        chrome.Network.setUserAgentOverride({ 'userAgent': networkOptions.userAgent });
      }
    });
  };
}

export async function configureChrome(chrome, functions: Array<IChromeOptionSetter> = []) {
  return new Promise((resolve, reject) => {
    Promise.all(functions.map(m => {
      m(chrome);
    })).then(() => {
      chrome.once('ready', function () {
        resolve(chrome);
      });
    }).catch(reject);
  });
}
