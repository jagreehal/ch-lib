import * as assert from 'assert';
import * as express from "express";
import {init} from './index';


describe('When initialising routes', () => {
    it('should be ok', () => {
      assert.ok(init());
    });
    it('can get routes', () => {
      assert.ok(init().routes);
    });

    it('can initial routes', () => {
      const router = express.Router();
      init().routes(router);
      assert.ok(router.stack.length > 0);
    });
});
