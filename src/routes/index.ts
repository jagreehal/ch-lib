import * as express from "express";
import { captureScreenshot } from "../screenshot";
import { getHar } from "../har";
import { captureScreencastAsync } from "../screencast";

export function init() {
  function foo(req, res, next) {
    return res.send('foo2');
  }
  async function bar(req, res, next) {
    let result = await captureScreenshot('http://www.bbc.co.uk/');
    return res.send(result);
  }

  async function har(req, res: express.Response, next) {
    // let url = req.query.url || '';
    // if (url){
    //   res.send('url is required').status(500);
    // }
    let result = await getHar('http://www.bbc.co.uk/');
    return res.send(result);
  }

  async function captureScreencast(req, res: express.Response, next) {
    // let url = req.query.url || '';
    // if (url){
    //   res.send('url is required').status(500);
    // }
    let result = await captureScreencastAsync('http://www.bbc.co.uk/');
    return res.send(result);
  }

  return {
    routes: (router: express.Router) => {
      router.get('/foo', foo);
      router.get('/bar', bar);
      router.get('/har', har);
      router.get('/screencast', captureScreencast);
    }
  }
};
